import React from "react";
import styles from "./InfoBox.module.css";

class InfoBox extends React.Component {
  constructor(props){
    super(props)
    this.state={
      ani:false,
    }
  }
  componentDidMount() {
    setTimeout(()=>{this.setState({ani:true})},0)
  }
  bookNow=(data)=>{
    this.props.btnClick(this.props.roomLabel,data)
  }
  createBedType = () => {
    const bed = Object.keys(this.props.data).map((key, index) => {
      return (
          <BedType label={key} data={this.props.data[key]} key={"bed" + index} btnClick={this.bookNow}/>
      );
    });
    return bed;
  };

  render() {
    return (
      <div className={styles.box+`${this.state.ani===true ? ' '+styles.boxAni:''}`} style={{'transitionDelay':Number(this.props.index)*.2+'s'}}>
        <div className={styles.roomTypeDiv}>{this.props.roomLabel}</div>
        <div className={styles.bedTypeDiv}>{this.createBedType()}</div>
      </div>
    );
  }
}
class BedType extends React.Component {
  componentDidMount() {
  }
  
  generateOption = () => {
    const option = this.props.data.data.map((val, index) => {
      return (
        <div className={styles.singleInfoRow} key={"option" + index}>
          <div className={styles.bedOption}>{val.boardCodeDescription}</div>
          <div className={styles.bedPrice}>
            Price: RM {val.totalPrice} 
            <button type="button" className="btn-primary" onClick={()=>{this.props.btnClick(val)}}>Click</button>
          </div>
          </div>
      );
    });
    return option;
  };
  render() {
    return (
      <div className={styles.bedTypeItem}>
        <div className={styles.bedLabel}>{this.props.label}</div>
        <div className={styles.bedInfo}>
            {this.generateOption()}
        </div>
        {/* <div className={styles.bedOption}>{this.generateOption()}</div>
        <div className={styles.bedPrice}>{this.generatePrice()}</div> */}
      </div>
    );
  }
}

export default InfoBox;
