import React from "react";
import styles from "./Notification.module.css";
class Notification extends React.Component {
  constructor(props){
    super(props)
    this.state={
      show:false,
      info:null,
      roomType:null
    }
    this.timer=null
  }
  componentDidMount(){
  }
  show=(roomType,info)=>{
    this.setState({show:true,info:info,roomType})
    clearTimeout(this.timer)
    this.timer=setTimeout(()=>{
      this.close()
    },5000)
  }
  close=()=>{
    this.setState({show:false})
  }
  render() {
    return (
      <div className={styles.notification+`${this.state.show===true ? ' '+styles.show:''}`} >
        <div className='bold'>Selected Item:</div>
        <hr/>
        <div>
          <div className="d-flex">
           <div className={styles.label}>Room Type:</div>
           <div className={styles.info}>{this.state.roomType?this.state.roomType:'n/a'}</div>
          </div>
          <div className="d-flex">
           <div className={styles.label}>Bed Type:</div>
           <div className={styles.info}>{this.state.info?this.state.info.bedTypeLabel.length>0?this.state.info.bedTypeLabel.join("/"):'Not Specific':'n/a'}</div>
          </div>
          <div className="d-flex">
           <div className={styles.label}>Option:</div>
           <div className={styles.info}>{this.state.info?this.state.info.boardCodeDescription:'n/a'}</div>
          </div>
          <div className="d-flex">
           <div className={styles.label}>Price:</div>
           <div className={styles.info}>{this.state.info?this.state.info.totalPrice:'n/a'}</div>
          </div>
        </div>
      </div>
    );
  }
}

export default Notification;
