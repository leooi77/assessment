import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
// import $ from "jquery";
// import Popper from "popper.js";
// import "bootstrap/dist/js/bootstrap.bundle.min";
import Header from "./components/Header/Header";
import InfoBox from "./components/InfoBox/InfoBox";
import Footer from "./components/Footer/Footer";
import Notification from "./components/Notification/Notification";
import _  from 'lodash'
const axios = require("axios");
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      serverData:null,
      roomList:[],
      bedList:[],
      optionList:[],
      filterRoom:"All",
      filterBed:"All",
      filterOption:"All"
    };
    this.notification=null
  }
  componentDidMount() {
    axios
      .get("test.json")
      .then(response => {
        this.generateData(response.data,false);
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  generateData = async(json,isFitler) => {
    await this.setState({data:null})
    let room = Object();
    let roomList=['All']
    let bedList=['All']
    let optionList=['All']
    json.forEach((val, index) => {
      if (val.roomTypeLabel) {
        val.roomTypeLabel.forEach((roomType, roomIndex) => {
          let roomLabel = roomType;
          if (!room[roomLabel]) {
            room[roomLabel] = Object();
            roomList.push(roomLabel)
          } else {
          }
          if (val.bedTypeLabel) {
            let myBedType = "Not Specific";
            if (val.bedTypeLabel.length > 0) {
              myBedType = val.bedTypeLabel.join("/");
              //val.bedTypeLabel.forEach((bedType,bedIndex) => {
              if (!room[roomLabel][myBedType]) {
                
                room[roomLabel][myBedType] = Object();
                room[roomLabel][myBedType].data = [];
                room[roomLabel][myBedType].data.push(val);
              } else {
                room[roomLabel][myBedType].data.push(val);
              }
              //})
            } else {
              if (!room[roomLabel][myBedType]) {
                
                room[roomLabel][myBedType] = Object();
                room[roomLabel][myBedType].data = [];
                room[roomLabel][myBedType].data.push(val);
              } else {
                room[roomLabel][myBedType].data.push(val);
              }
            }
            if(_.indexOf(bedList,myBedType)<0){
              bedList.push(myBedType)
            }
          }
          if(_.indexOf(optionList,val.boardCodeDescription)<0){
            optionList.push(val.boardCodeDescription)
          }
        });
      }
    });
    console.log(room)
   
    if(isFitler){
      this.setState({ data: room});
    }else{
      this.setState({ serverData:json, data: room,roomList,bedList,optionList });
    }
    
    
  };
  displayInfo = () => {
    if (this.state.data) {
      const infos = Object.keys(this.state.data).map((key,index) => {
        return (
          <InfoBox
            index={index}
            roomLabel={key}
            data={this.state.data[key]}
            key={"info" + key}
            btnClick={this.showNotification}
          />
        );
      });
      if(infos.length<=0){
          return <div>No Recound Found</div>;
      }
      return infos;
    }
    return null
  };
  filter=()=>{
    console.log(this.state.serverData)
    let result= JSON.parse(JSON.stringify(this.state.serverData))
    if(this.state.filterRoom!=='All'){
      result=result.filter((val,index)=>{
        return _.indexOf(val.roomTypeLabel,this.state.filterRoom)>=0
      })
    }
    if(this.state.filterBed!=='All'){
      result=result.filter((val,index)=>{
        if(val.bedTypeLabel.length>1){
          return (val.bedTypeLabel.join('/')===this.state.filterBed)
        }
        if(val.bedTypeLabel.length===0){
          return (this.state.filterBed==='Not Specific')
        }
        //if(val.bedTypeLabel.length===1){
          return (val.bedTypeLabel.join('')===this.state.filterBed)
        //}
      })
    }
    if(this.state.filterOption!=='All'){
      result=result.filter((val,index)=>{
        return (val.boardCodeDescription===this.state.filterOption)
      })
    }
    this.generateData(result,true)
    console.log(result)
  }
  showNotification=(roomType,index)=>{
    this.notification.show(roomType,index)
  }
  onFilterRoomChange=(event)=>{
    this.setState({filterRoom: event.target.value},()=>{this.filter()});
  }
  onFilterBedChange=(event)=>{
    this.setState({filterBed: event.target.value},()=>{this.filter()});
  }
  onFilterOptionChange=(event)=>{
    this.setState({filterOption: event.target.value},()=>{this.filter()});
  }
  render() {
    const room = this.state.roomList.map((val,index)=>{
      return <option key={'room'+index}>{val}</option>
    });
    const bed = this.state.bedList.map((val,index)=>{
      return <option key={'bed'+index}>{val}</option>
    });
    const option = this.state.optionList.map((val,index)=>{
      return <option key={'option'+index}>{val}</option>
    });
    return (
      // <HashRouter history={ HashRouter.hashHistory }>
      <React.Fragment>
        <div className="app d-flex flex-column">
         
            <Header />
            <div className="content">
              <div className="filterBox">
                <div className="filterRow">
                  <div className="filterLabel">Room Type</div>
                  <select className="form-control filterSelect" id="roomFilter" onChange={this.onFilterRoomChange} value={this.state.filterRoom}>
                    { room }
                  </select>
                </div>
                <div className="filterRow">
                  <div className="filterLabel">Bed Type</div>
                  <select className="form-control filterSelect" id="bedFilter" onChange={this.onFilterBedChange} value={this.state.filterBed}>
                    { bed }
                  </select>
                </div>
                <div className="filterRow">
                  <div className="filterLabel">Option</div>
                  <select className="form-control filterSelect" id="optionFilter" onChange={this.onFilterOptionChange} value={this.state.filterOption}>
                    { option }
                  </select>
                </div>
              </div>
              {this.displayInfo()}
            </div>
          
          
         
          <Notification ref={(elm)=>{this.notification=elm}}/>
        </div>
         <Footer/>
         </React.Fragment>
        // </HashRouter> 
    );
  }
}

export default App;
